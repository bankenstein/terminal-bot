
#include <iostream>
#include <string>

using namespace std;

int main () {
    int n = 10;
    string s;
    //create a dynamic array with length of n
    int *arr = new int[n];
    for ( int i = 0; i < n; i++ ) {
        arr[i] = i + 1;
    }
    s = string(arr, ' ');
    
    cout << s << endl;
    return 0;
}