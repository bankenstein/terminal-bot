#!/bin/bash

cp run.sh ../runBackUp.sh
cp wipe.sh ../wipe.sh
cd ..
sudo rm -rvf ./fields
mkdir fields
cp runBackUp.sh ./fields/run.sh
cp wipe.sh ./fields/wipe.sh
rm -rvf runBackUp.sh
rm -rvf wipe.sh
