const http = require('https');
module.exports = {
    name: 'reddit',
    permission: 'READ_MESSAGE',
    description: 'Reddit api. Search stuff on Reddit.',
    usage: 'reddit',
    execute: (message, args, sudo) => {
        if (args.length < 1) {
            return message.channel.send('You need to specify something to search for.');
        }
        const query = args.join(' ');
        const url = `https://www.reddit.com/r/memes/search.json?q=${encodeURIComponent(query)}&sort=relevance&restrict_sr=1`;
        http.get(url, (res) => {
            res.setEncoding('utf8');
            let body = '';
            res.on('data', (chunk) => {
                body += chunk;
            });
            res.on('end', () => {
                try {
                    const data = JSON.parse(body);
                    if (data.data.children.length === 0) {
                        return message.channel.send('No results found.');
                    }
                    const index = Math.floor(Math.random() * data.data.children.length);
                    const embed = {
                        title: data.data.children[index].data.title,
                        url: `https://reddit.com${data.data.children[index].data.permalink}`,
                        description: `${data.data.children[index].data.selftext}`,
                        color: 0x00ff00,
                        timestamp: new Date(),
                        footer: {
                            text: `Reddit (${data.data.children[index].data.author}) Post: ${index}/${data.data.children.length}`
                        },
                        image: {
                            url: data.data.children[index].data.url
                        },
                        timestamp: new Date(data.data.children[index].data.created_utc * 1000)
                    }
                    return message.channel.send({ embeds: [embed] });
                } catch (e) {
                    console.log(e.message)
                    return message.channel.send('An error occurred.');
                }
            });
        }).on('error', (e) => {
            console.error(e);
            return message.channel.send('An error occurred.');
        });
    }

}