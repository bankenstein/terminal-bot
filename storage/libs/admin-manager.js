module.exports = {
    name: 'admin-manager',
    permission: 'ADMINISTRATOR',
    description: 'Admin manager (sudo only)',
    usage: 'admin',
    execute: (message, args, sudo, more) => {
        if (!sudo) return message.channel.send("You don't have permission to use this command (sudo required)");
        if (args.length < 1) {
            message.channel.send('Please provide a command to execute.');
            return;
        }
        const command = args[0];
        args.shift();
        switch (command) {
            case '-r':
                if (args.length < 1) {
                    message.channel.send('Please provide a operation.');
                    return;
                }
                switch (args[0]) {
                    case "-l":
                        let out = ""
                        message.guild.roles.cache.forEach(r => {
                            out += `__${r.name}__\n`;
                        });
                        message.channel.send({
                            embeds: [{
                                color: 0x00ff00,
                                title: "Roles",
                                description: out
                            }]
                        });
                        break;
                    case 'add':
                        if (args.length < 2) {
                            message.channel.send('Please provide a role name and a permission (optional).');
                            return;
                        }
                        const roleName = args[1];
                        const permission = args[3] || 'READ_MESSAGES';
                        const color = parseInt(args[2]) || 'DEFAULT';
                        const role = message.guild.roles.cache.find(r => r.name === roleName);
                        if (role) {
                            message.channel.send('Role already exists.');
                            return;
                        }
                        message.guild.roles.create({
                            name: roleName,
                            color: color
                        }).then(r => {
                            message.channel.send(`Role ${roleName} created with color ${color} and permission ${permission}.`);
                        }).catch(err => {
                            message.channel.send(`Error: ${err}`);
                        });
                        break;
                    case 'rm':
                        if (args.length < 2) {
                            message.channel.send('Please provide a role name.');
                            return;
                        }
                        const rn = args[1];
                        const r = message.guild.roles.cache.find(r => r.name === rn);
                        if (!r) {
                            message.channel.send('Role does not exist.');
                            return;
                        }
                        r.delete().then(r => {
                            message.channel.send(`Role ${rn} removed.`);
                        }).catch(err => {
                            message.channel.send(`Error: ${err}`);
                        });
                        break;
                    case "addm":
                        if (args.length < 2) {
                            message.channel.send('Please provide a role name and a member name.');
                            return;
                        }
                        const rn2 = args[1].substring(3, args[1].length - 1);
                        const r2 = message.guild.roles.cache.find(r => r.id === rn2);
                        if (!r2) {
                            message.channel.send('Role does not exist.');
                            return;
                        }
                        const mn = args[2].substring(3, args[2].length - 1);
                        const m = message.guild.members.cache.find(m => m.id === mn);
                        console.log(message.guild.members.cache)
                        if (!m) {
                            message.channel.send('Member does not exist.');
                            return;
                        }
                        m.roles.add(r2).then(m => {
                            message.channel.send(`Member ${m.user.username} added to role ${r2.name}.`);
                        }).catch(err => {
                            message.channel.send(`Error: ${err}`);
                        });
                        break;
                    case "rmm":
                        if (args.length < 2) {
                            message.channel.send('Please provide a role name and a member name.');
                            return;
                        }
                        const rn3 = args[1].substring(3, args[1].length - 1);
                        const r3 = message.guild.roles.cache.find(r => r.id === rn3);
                        if (!r3) {
                            message.channel.send('Role does not exist.');
                            return;
                        }
                        const mn2 = args[2].substring(3, args[2].length - 1);
                        const m2 = message.guild.members.cache.find(m => m.user.id === mn2);
                        if (!m2) {
                            message.channel.send('Member does not exist.');
                            return;
                        }
                        m2.roles.remove(r3).then(m => {
                            message.channel.send(`Member ${m2.user.username} removed from role ${r3.name}.`);
                        }).catch(err => {
                            message.channel.send(`Error: ${err}`);
                        });
                        break;
                    default:
                        message.channel.send('Please provide a valid command.');
                        break;
                }
                break;
            case '-c':
                if (args.length < 2) {
                    message.channel.send('Please provide a channel name and a permission.');
                    return;
                }
                switch (args[0]) {
                    case 'add':
                        if (args.length < 2) {
                            message.channel.send('Please provide a channel name and a permission.');
                            return;
                        }
                        const channelName = args[1];
                        const permission = args[2];
                        args.shift();
                        args.shift();
                        const channel = message.guild.channels.cache.find(c => c.name === channelName);
                        if (channel) {
                            message.channel.send('Channel already exists.');
                            return;
                        }
                        message.guild.channels.create(channelName).then(c => {
                            message.channel.send(`Channel ${channelName} created with permission ${permission}.`);
                        }).catch(err => {
                            message.channel.send(`Error: ${err}`);
                        });
                        break;
                    case 'rm':
                        if (args.length < 2) {
                            message.channel.send('Please provide a channel name.');
                            return;
                        }
                        const cn = args[1].substring(2, args[1].length - 1);
                        const c = message.guild.channels.cache.find(c => c.id === cn);
                        if (!c) {
                            message.channel.send('Channel does not exist.');
                            return;
                        }
                        c.delete().then(c => {
                            message.channel.send(`Channel ${cn} removed.`);
                        }).catch(err => {
                            message.channel.send(`Error: ${err}`);
                        });
                        break;
                    case "-m":
                        if (args.length < 2) {
                            message.channel.send('Please provide a channel name and a member name.');
                            return;
                        }
                        const cn2 = args[1].substring(2, args[1].length - 1);
                        const c2 = message.guild.channels.cache.find(c => c.id === cn2);
                        if (!c2) {
                            message.channel.send('Channel does not exist.');
                            return;
                        }
                        const mn = args[2].substring(3, args[2].length - 1);
                        console.log(mn)
                        const m = message.guild.members.cache.find(m => m.id == mn);
                        const perm = args[3];
                        const set = args[4];
                        let obj = {}
                        obj[perm] = set;
                        if (!m) {
                            message.channel.send('Member does not exist.');
                            return;
                        }
                        c2.permissionOverwrites.edit(m, obj).then(c => {
                            message.channel.send(`Changed ${mn}'s permissions for channel ${cn2}.`);
                        }).catch(err => {
                            message.channel.send(`Error: ${err}`);
                        });
                        break;
                }
                break;
            case '-u':
                if (args.length < 2) {
                    message.channel.send('Please provide a user name and a permission.');
                    return;
                }
                switch (args[0]) {
                    case 'kick':
                        if (args.length < 3) {
                            message.channel.send('Please provide a user name and a reason.');
                            return;
                        }
                        const un = args[1].substring(3, args[1].length - 1);
                        console.log(un)
                        const u = message.guild.members.cache.find(u => u.id === un);
                        if (!u) {
                            message.channel.send('User does not exist.');
                            return;
                        }
                        const r = args.slice(2).join(' ');
                        u.kick(r).then(u => {
                            message.channel.send(`User ${u.user.username} kicked.`);
                        }).catch(err => {
                            message.channel.send(`Error: ${err}`);
                        });
                        break;
                    case 'ban':
                        if (args.length < 3) {
                            message.channel.send('Please provide a user name and a reason.');
                            return;
                        }
                        const un2 = args[1].substring(3, args[1].length - 1);
                        const u2 = message.guild.members.cache.find(u => u.id === un2);
                        if (!u2) {
                            message.channel.send('User does not exist.');
                            return;
                        }
                        const r2 = args.slice(2).join(' ');
                        u2.ban(r2).then(u => {
                            message.channel.send(`User ${u2.user.username} banned.`);
                        }).catch(err => {
                            message.channel.send(`Error: ${err}`);
                        });
                        break;
                    case "-r":
                        if (args.length < 3) {
                            message.channel.send('Please provide a user name and a new name.');
                            return;
                        }
                        const un3 = args[1].substring(3, args[1].length - 1);
                        const u3 = message.guild.members.cache.find(u => u.user.id === un3);
                        if (!u3) {
                            message.channel.send('User does not exist.');
                            return;
                        }
                        const nn = args[2];
                        u3.setNickname(nn).then(u => {
                            message.channel.send(`User ${u3.user.username} renamed to ${nn}.`);
                        }).catch(err => {
                            message.channel.send(`Error: ${err}`);
                        });
                        break;

                }
                break;
            case '--help':
                message.channel.send({
                    embeds: [{
                        color: 0x00ff00,
                        title: "Help",
                        description: "```\n" +
                            "Syntax: admin <command_type> <command> <args>\n" +
                            "Command types:\n" +
                            "  -r     role\n" +
                            "  -c     channel\n" +
                            "  --help help\n" +
                            "  -u     user\n" +
                            "Commands for **role**:\n" +
                            "  add <role_name> <color?> <permissions?> - adds a new role with this color\n" +
                            "  rm <role_name>\n" +
                            "  addm <role_name> <member_name> - add this role for the member\n" +
                            "  rmm <role_name> <member_name> - remove this role for the member\n" +
                            "  -l - lists all of the roles in the server\n" +
                            "Commands for **channel**:\n" +
                            "  add <channel_name> <permissions?> - adds a channel with name and permision\n" +
                            "  rm <channel_name> - removes the channel\n" +
                            "  -m <channel_name> <member_name> <permission> <set> - gives specific permision for the member on this channel\n" +
                            "Commands for **user**:\n" +
                            "  kick <user_name> <reason>\n" +
                            "  ban <user_name> <reason>\n" +
                            "```"
                    }]
                })
        }
    }
}