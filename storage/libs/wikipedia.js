const http = require('https');

module.exports = {
    name: 'wikipedia',
    permission: 'READ_MESSAGE',
    description: 'Wikipedia api. Search stuff on wikipedia.',
    usage: 'wiki',
    execute: (message, args, sudo) => {
        console.log("asd")
        if (args.length < 1) {
            return message.channel.send('You need to specify something to search for.');
        }
        const query = args.join(' ');
        const url = `https://en.wikipedia.org/w/api.php?action=query&list=search&srsearch=${encodeURIComponent(query)}&format=json&origin=*`;
        // fetch with http
        http.get(url, (res) => {
            res.setEncoding('utf8');
            let body = '';
            res.on('data', (chunk) => {
                body += chunk;
            });
            res.on('end', () => {
                try {
                    const data = JSON.parse(body);
                    if (data.query.searchinfo.totalhits === 0) {
                        return message.channel.send('No results found.');
                    }
                    const snippet = data.query.search[0].snippet.replace(/<\/?[^>]+(>|$)/g, '');
                    const embed = {
                        title: data.query.search[0].title,
                        url: `https://en.wikipedia.org/wiki/${encodeURIComponent(data.query.search[0].title)}`,
                        description: snippet,
                        color: 0x00ff00,
                        timestamp: new Date(),
                        footer: {
                            text: 'Wikipedia'
                        },
                        timestamp: new Date(data.query.search[0].timestamp)
                    }
                    return message.channel.send({ embeds: [embed] });
                } catch (e) {
                    console.log(e.message)
                    return message.channel.send('An error occurred.');
                }
            });
        }).on('error', (e) => {
            console.error(e);
            return message.channel.send('An error occurred.');
        });
    }
}
