const request = require('request');

module.exports = {
    name: 'math',
    permission: 'READ_MESSAGE',
    description: 'Calculate stuff.',
    usage: 'math',
    execute: (message, args, sudo) => {
        if (args.length < 1) {
            return message.channel.send('You need to specify something to calculate.');
        }
        const query = args.join(' ');
        //wolfram alpha
        const url = `https://www.wolframalpha.com/input?i=${encodeURIComponent(query)}`;


        // request(url, (error, response, body) => {
        //     if (error) {
        //         return message.channel.send('Something went wrong.');
        //     }
        //     // get the element "section" with class _1vuO
        //     console.log(body)
        //     const section = body.match(/<section class="_1vuO">(.*?)<\/section>/g);
        //     if (!section) {
        //         return message.channel.send('I couldn\'t find a result.');
        //     }
        //     // split the result into an array and every element should be <section tabindex="0" class="_gtUC _3WIS">;
        //     const result = section[0].split('<section tabindex="0" class="_gtUC _3WIS">');
        //     // remove the html tags and images and if there is a image - create a shortcut to the url
        //     const answer = result.map(e => e.replace(/<\/?[^>]+(>|$)/g, '').replace(/<img.*?src="(.*?)".*?>/g, '$1'));
        //     // send the answers in fields of embed
        //     message.channel.send({
        //         embed: {
        //             color: 0x00ff00,
        //             title: 'Wolfram Alpha',
        //             description: answer.join('\n'),
        //             footer: {
        //                 text: 'Powered by Wolfram Alpha'
        //             }
        //         }
        //     });
        // })
    }
}