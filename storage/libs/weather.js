const weather = require('weather-js');
function capitalFirst(str) {
    str += '';
    return str.charAt(0).toUpperCase() + str.slice(1);
}
module.exports = {
    name: 'Weather',
    permission: 'READ_MESSAGES',
    description: 'Weather',
    usage: 'weather',
    execute: (message, args, sudo, more) => {
        let place = args.join(' ');
        if (!place) return message.channel.send('Please specify a location.');
        weather.find({ search: place, degreeType: 'C' }, function (err, result) {
            if (err) console.log(err);
            let response = result[0];
            if (response) {
                message.channel.send({
                    embeds: [{
                        color: '#FFFF00',
                        title: 'Weather',
                        fields: [
                            { name: 'Location', value: response.current.observationpoint },
                            {
                                name: 'Coordinates', value: "Lattitude - " + Math.abs(response.location.lat) + (response.location.lat > 0 ? '° N' : '° S') +
                                    ", Longitude - " + Math.abs(response.location.long) + (response.location.long > 0 ? '° E' : '° W')
                            },
                            { name: 'Timezone ', value: "UTC" + (response.location.timezone > 0 ? '+' : '') + response.location.timezone },
                            { name: 'Weather ', value: capitalFirst(response.current.skytext) },

                            { name: 'Current Temperature ', value: response.current.temperature + '°C' },
                            { name: 'Feels like:  ', value: response.current.feelslike + '°C' },
                            { name: 'Wind ', value: response.current.winddisplay + '' },
                            { name: 'Time ', value: response.current.day + ' ' + response.current.date + ' ' + response.current.observationtime },
                        ],
                    }]
                })
            }
        })

    }
}