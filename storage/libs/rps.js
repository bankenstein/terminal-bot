module.exports = {
    name: "rps",
    permission: "READ_MESSAGE",
    description: "Rock, paper, scissors",
    usage: "rps",
    execute: async (message, args, sudo) => {
        const embed = {
            title: "Rock, paper, scissors",
            description: "Rock beats scissors, scissors beats paper, paper beats rock.",
            color: 0x00ff00
        }
        let msg = await message.channel.send({ embeds: [embed] })
        const move = ["✊", "✋", "✌"][Math.floor(Math.random() * 3)];
        msg.react("✊");
        msg.react("✋");
        msg.react("✌");
        const filter = (reaction, user) => {
            return ['✊', '✋', '✌'].includes(reaction.emoji.name) && user.id === message.author.id;
        };
        let reacted = 0
        console.log(move)
        const collector = msg.createReactionCollector(filter, { time: 60000 });
        collector.on('collect', async (reaction, user) => {
            if (user.bot) return;
            if (reacted == 1) return;
            reacted++;
            let result = ""
            if (move === reaction.emoji.name) {
                result = "draw";
            } else if ((move === '✊' && reaction.emoji.name === '✋') || (move === '✌' && reaction.emoji.name === '✊') || (move === "✋" && reaction.emoji.name === '✌')) {
                result = "won";
            } else {
                result = "lost";
            }
            msg.edit({
                embeds: [{
                    title: "Rock, paper, scissors",
                    description: `${result != "draw" ? `You ${result}` : `Draw`}! The computer chose ${move}`,
                    color: result == "lost" ? 0xff0000 : 0x00ff00
                }]
            });
        })
        collector.on('end', collected => {
            msg.edit({
                embed: {
                    title: "Rock, paper, scissors",
                    description: "You ran out of time!",
                    color: 0xff0000
                }
            })
        })
    }
}