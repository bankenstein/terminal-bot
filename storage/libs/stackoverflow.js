const http = require('request');
module.exports = {
    name: 'stackoverflow',
    permission: 'READ_MESSAGE',
    description: 'Stackoverflow api. Search stuff on stackoverflow.',
    usage: 'stackoverflow',
    execute: (message, args, sudo) => {
        // stackoverflow api
        if (args.length < 1) {
            return message.channel.send('You need to specify something to search for.');
        }
        const query = args.join(' ');
        const url = `https://api.stackexchange.com/2.2/search/advanced?order=desc&sort=activity&q=${encodeURIComponent(query)}&site=stackoverflow`;
        console.log(url)
        http({
            url: url,
            gzip: true
        }, (err, res, body) => {
            if (err) {
                console.log(err)
                return message.channel.send('An error occurred while connecting.');
            }
            try {
                const data = JSON.parse(body);
                if (data.items.length === 0) {
                    return message.channel.send('No results found.');
                }
                const embed = {
                    title: data.items[0].title,
                    url: data.items[0].link,
                    description: data.items[0].body,
                    color: 0x00ff00,
                    timestamp: new Date(),
                    footer: {
                        text: `Stackoverflow (${data.items[0].owner.display_name})`
                    },
                    timestamp: new Date(data.items[0].creation_date)
                }
                let index = 0;
                // react to this message with > and < and navigate through the results
                message.channel.send({ embeds: [embed] }).then(async (msg) => {
                    msg.react('⬅')
                    msg.react('➡')
                    const filter = (reaction, user) => {
                        return ['⬅', '➡'].includes(reaction.emoji.name) && user.id === message.author.id;
                    };
                    const collector = msg.createReactionCollector(filter, { time: 60000 });
                    collector.on('collect', async (reaction, user) => {
                        if (user.bot) return;
                        if (reaction.emoji.name === '➡') {
                            index++;
                            index = index % data.items.length;
                        } else if (reaction.emoji.name === '⬅') {
                            index--;
                            index = index < 0 ? data.items.length - 1 : index;
                        }
                        const userReactions = msg.reactions.cache.filter(reaction => reaction.users.cache.has(user.id));

                        try {
                            for (const reaction of userReactions.values()) {
                                await reaction.users.remove(user.id);
                            }
                        } catch (error) {
                            console.error('Failed to remove reactions.');
                        }
                        embed.title = data.items[index].title;
                        embed.url = data.items[index].link;
                        embed.description = data.items[index].body;
                        embed.footer.text = `Stackoverflow (${data.items[index].owner.display_name}). Index ${index + 1}/${data.items.length}`;
                        embed.timestamp = new Date(data.items[index].creation_date);
                        msg.edit({ embeds: [embed] });
                    });
                    collector.on('end', (collected) => {
                        msg.reactions.removeAll();
                    });
                })
            } catch (e) {
                console.log(e.message, res)
                return message.channel.send('An error occurred.');
            }
        })
    }
}