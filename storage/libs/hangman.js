module.exports = {
    name: 'hangman',
    permission: 'READ_MESSAGE',
    description: 'Hangman game',
    usage: 'hangman',
    execute: (message, args, sudo) => {
        // get a word list
        const words = require('./word.json');
        let word = "";
        if (args.indexOf("--hard") != -1) {
            // get a hard word list
            const w = words.filter(w => w.length > 10)
            word = w[Math.floor(Math.random() * w.length)];
        } else {
            // get a random word
            word = words[Math.floor(Math.random() * words.length)];
        }
        // create an array of underscores
        console.log(word)
        let res = "";
        res = word.replace(/[a-zA-Z]/g, "_")
        let limit = parseInt(word.length * 1.5) > 20 ? 20 : parseInt(word.length * 1.5);
        let trys = limit;
        let letters = [];
        let won = false;
        message.channel.send(`Tries: ${trys} \`\`\`${res.split('').join(' ')}\`\`\``).then(msg => {
            const collector = message.channel.createMessageCollector(m => m.author.id === message.author.id && !m.author.bot, { time: 60000 });
            collector.on('collect', m => {
                if (won) return;
                var got = false;
                switch (args[0]) {
                    case "--all":
                        break;
                    default:
                        if (m.author.id != message.author.id) {
                            return;
                        }
                        break;
                }
                if (m.content.length != 1) {
                    if (m.content.replace(" ", "").toLowerCase() == word) {
                        won = true;
                        return msg.edit("You won! The word was " + word + "! You tried " + (limit - trys) + " times.");
                    } else {
                        return;
                    }
                }
                for (let i = 0; i < word.length; i++) {
                    if (word[i] === m.content) {
                        res = res.split("")
                        res[i] = m.content;
                        res = res.join("")
                        got = true;
                    }
                }
                if (res == word) {
                    won = true;
                    return msg.edit("You won! The word was " + word + "! You tried " + (limit - trys) + " times.");
                }
                if (!got && letters.indexOf(m.content) == -1) {
                    trys--;
                    letters.push(m.content);
                    if (trys <= 0) {
                        won = true;
                        return msg.edit("You lost! The word was " + word + "!");
                    }
                }
                // edit the message with the new word
                msg.edit(`try: ${trys} \`\`\`${res.split("").join(" ")}\n${letters.join(" ")}\`\`\``);
            })
        })
    }
}