const type = ["🂡", "🂱", "🃁", "🃑", "🂢", "🂲", "🃂", "🃒", "🂣", "🂳", "🃃", "🃓", "🂤", "🂴", "🃄", "🃔", "🂥", "🂵", "🃅", "🃕", "🂦", "🂶", "🃆", "🃖", "🂧", "🂷", "🃇", "🃗", "🂨", "🂸", "🃈", "🃘", "🂩", "🂹", "🃉", "🃙", "🂪", "🂺", "🃊", "🃚", "🂫", "🂻", "🃋", "🃛", "🂭", "🂽", "🃍", "🃝", "🂮", "🃞"];

function random(from, to) {
    return Math.floor(Math.random() * (to - from)) + from;
}
class Player {
    constructor(id) {
        this.id = id;
        this.hand = [];
        this.score = 0;
    }

    addCard(card) {
        this.hand.push(card);
        this.score += card.value;
    }

    getScore() {
        return this.score;
    }

    getHand() {
        return this.hand;
    }

    getName() {
        return this.name;
    }
}
class Lobby {
    constructor() {
        this.name = "";
        this.players = [];
        this.maxPlayers = 0;
        this.minPlayers = 0;
        this.game = null;
        this.host = null;
    }
    setHost(user) {
        this.host = user;
    }
    addPlayer(user) {
        this.players.push(user);
    }
    removePlayer(user) {
        this.players.splice(this.players.indexOf(user), 1);
    }
    changeGameStatus() {
        this.game = !this.game;
    }
    setName(name) {
        this.name = name;
    }
    setMaxPlayers(max) {
        this.maxPlayers = max;
    }
    setMinPlayers(min) {
        this.minPlayers = min;
    }
    giveCards(cards) {
        for (let i = 0; i < 2; i++) {
            this.players.map(p => {
                p.addCard(cards.splice(random(0, cards.length), 1)[0]);
                cards.push(p.hand[p.hand.length])
            });
        }
        return cards;
    }
}
var lobby;
var cards = [];
// fill cards with all cards
for (let i = 0; i < type.length; i++) {
    cards = type[i];
}
for (let i = 0; i < cards.length; i++) {
    cards.push(cards.splice(random(0, cards.length - i), 1)[0]);
}

module.exports = {
    name: 'blackjack',
    permission: 'READ_MESSAGE',
    description: 'Blackjack',
    usage: 'bj',
    execute: (message, args, sudo) => {
        switch (args[0]) {
            case "lobby":
                switch (args[1]) {
                    case "create":
                        if (lobbies.length >= 10) {
                            return message.channel.send("There are too many lobbies.");
                        }
                        lobby = new Lobby()
                        lobby.setHost(msg.author.id)
                        break;
                    case "join":
                        lobby.addPlayer(msg.author.id)
                        break;
                    case "leave":
                        lobby.removePlayer(msg.author.id)
                        break;
                    case "start":
                        lobby.changeGameStatus()
                        cards = lobby.giveCards(cards)
                        break;
                    case "config":
                        lobby.setName(args[3] ? args[3] : `bj-lobby${lobbies.indexOf(l)}`)
                        lobby.setMaxPlayers(args[4] ? args[4] : 5)
                        lobby.setMinPlayers(args[5] ? args[5] : 1)
                        break;
                    default:
                        return message.channel.send("Invalid command.");
                }
                break;
            case "take":
                // take a card

                break;
        }
    }
}