const ytdl = require('ytdl-core');
const queue = new Map();
const { joinVoiceChannel } = require("@discordjs/voice");

var index = 0, loopall = false, loop = false;
async function voiceCommands(message, args) {
    if (message.author.bot) return;
    const serverQueue = queue.get(message.guild.id);

    switch (args[0]) {
        case "play":
            execute(message, serverQueue);
            break;
        case "skip":
            skip(message, serverQueue);
            loop = false;
            break;
        case "stop":
            stop(message, serverQueue);
            loop = false;
            loopall = false;
            index = 0
            break;
        case "pause":
            pause(serverQueue)
            break;
        case "resume":
            resume(serverQueue)
            break;
        case "volume":
            volume(serverQueue, message, currentChannel)
            break;
        case "loop":
            loop = true;
            message.channel.send("Looping current music.")
            break;
        case "loopall":
            loopall = true;
            message.channel.send("Looping the queue.")
            break;
    }
}

async function execute(message, serverQueue) {
    const args = message.content.split(" ");
    args.shift()
    args.shift()
    args.shift()

    var voiceChannel = message.member.voice.channel;

    if (!voiceChannel)
        return message.channel.send(
            "You need to be in a voice channel to play music!"
        );
    const permissions = voiceChannel.permissionsFor(message.client.user);
    if (!permissions.has("CONNECT") || !permissions.has("SPEAK")) {
        return message.channel.send(
            "I need the permissions to join and speak in your voice channel!"
        );
    }

    const songName = args.join(" ");
    var songInfo, song;
    try {
        songInfo = await ytdl.getInfo(songName)
        song = {
            title: songInfo.videoDetails.title,
            url: songInfo.videoDetails.video_url,
        };
    } catch (err) {
        message.channel.send("There was something wrong when fetching info: ```" + err.message + "\n" + songName + "```")
    }

    if (!serverQueue) {
        const queueContruct = {
            textChannel: message.channel,
            voiceChannel: voiceChannel,
            connection: null,
            songs: [],
            volume: 1,
            playing: true
        };

        queue.set(message.guild.id, queueContruct);

        queueContruct.songs.push(song);

        try {

            var connection = await joinVoiceChannel(
                {
                    channelId: message.member.voice.channel,
                    guildId: message.guild.id,
                    adapterCreator: message.guild.voiceAdapterCreator
                });
            console.log(connection)
            queueContruct.connection = connection;
            // play(message.guild, queueContruct.songs[0]);
        } catch (err) {
            console.log(err.message);
            queue.delete(message.guild.id);
            return message.channel.send(err.message);
        }
    } else {
        serverQueue.songs.push(song);
        return message.channel.send({
            embed: {
                title: `${song.title} has been added to the queue!`,
                color: color.RED,
                description: `URL: ${song.url}`
            }
        });
    }
}

function skip(message, serverQueue) {
    if (!message.member.voice.channel)
        return message.channel.send(
            "You have to be in a voice channel to stop the music!"
        );
    if (!serverQueue)
        return message.channel.send("There is no song that I could skip!");
    serverQueue.connection.dispatcher.end();
}

function stop(message, serverQueue) {
    if (!message.member.voice.channel)
        return message.channel.send(
            "You have to be in a voice channel to stop the music!"
        );

    if (!serverQueue)
        return message.channel.send("There is no song that I could stop!");

    serverQueue.songs = [];
    serverQueue.connection.dispatcher.end();
}
function pause(serverQueue) {
    serverQueue.connection.dispatcher.pause();
}
function resume(serverQueue) {
    serverQueue.connection.dispatcher.resume()
}
function volume(serverQueue, message, currentChannel) {
    var volume = Number(message.content.replace(`${prefix[currentChannel]}volume `, ""))
    volume = (volume > 100 ? 100 : (volume < 1 ? 1 : volume))
    volume = isNaN(volume) ? 1 : volume
    serverQueue.volume = volume / 100;
    serverQueue.connection.dispatcher.setVolumeLogarithmic(serverQueue.volume)
    message.channel.send(`Volume changed to: ${volume}%`)
}

function play(guild, song) {
    const serverQueue = queue.get(guild.id);
    if (!song) {
        serverQueue.voiceChannel.leave();
        queue.delete(guild.id);
        return;
    }

    const dispatcher = serverQueue.connection
        .play(ytdl(song.url))
        .on("finish", () => {
            if (!loop && !loopall) {
                serverQueue.songs.shift();
            }
            if (loopall) {
                index++;
                index = index % serverQueue.songs.length
            }
            play(guild, serverQueue.songs[index]);
        })
        .on("error", error => console.error(error));
    dispatcher.setVolumeLogarithmic(serverQueue.volume);
    if (!loop) {
        serverQueue.textChannel.send({
            embed: {
                title: `Start playing: **${song.title}**`,
                color: color.RED,
                description: `URL: ${song.url}`
            }
        });
    }
}

module.exports = {
    name: 'music',
    description: 'Music commands',
    usage: 'music',
    execute: voiceCommands
}