const { MinecraftServerListPing, MinecraftQuery } = require("minecraft-status");
module.exports = {
	name: 'minecraft',
	permission: 'READ_MESSAGE',
	description: 'Minecraft server status',
	usage: 'mc',
	execute: (message, args) => {
		if (!args[0]) return message.channel.send('What about minecraft?')
		if (args[0] === 'smp') {
			args[0] = "213.214.94.248";
		}
		if (args[0] === 'hypixel') args[0] = "mc.hypixel.net";
		if (!args[1]) args[1] = 25565;
		MinecraftServerListPing.ping(4, args[0], parseInt(args[1]), 300)
			.then(response => {
				message.channel.send({
					embeds: [{
						color: '#197103',
						title: 'Minecraft Server Status',
						fields: [
							{ name: 'Server IP:', value: args[0] },
							{ name: 'Online Players:', value: response.players.online.toString() },
							{ name: 'Max Players:', value: response.players.max.toString() },
							{ name: 'Minecraft Version:', value: response.version.name.toString() },

						],
					}]
				});

			})
			.catch(error => {
				console.log(error);
			});
	}
}
