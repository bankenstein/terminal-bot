import requests
import json
from bs4 import BeautifulSoup
import sys

string = sys.argv[1]
lang = sys.argv[2]

url = "https://translate.google.bg/?hl={}&sl=auto&tl={}&text={}&op=translate".format(
    lang, lang, string.replace(" ", "%20"))

site = requests.get(url)
soup = BeautifulSoup(site.text, "html.parser")
spans = soup.find_all('span')

for span in spans:
    print(span)
    if 'jsaction="click:qtZ4nf,GFf3ac,tMZCfe; contextmenu:Nqw7Te,QP7LD; mouseout:Nqw7Te; mouseover:qtZ4nf,c2aHje" jsname="W297wb"' in span:
        print(span[span.index(">") + 1:-7])


with open("latest-report-translate.py.log", "w") as f:
    f.write("String: {},\nlanguage: {},\nHTML Response: \n{}".format(
        string, lang, soup))
print("Span not found")
