const fs = require("fs");
const { exec } = require('child_process');

// check if all files in ./storage/systems/ are not empty
const checkSystems = () => {
    const systems = fs.readdirSync("./storage/systems/");
    let stat = "";
    for (let i = 0; i < systems.length; i++) {
        const system = systems[i];
        const systemPath = `./storage/systems/${system}`;
        const systemFiles = fs.readFileSync(systemPath);
        if (systemFiles.length === 0) {
            console.log(`${system} is empty. Attempting to fix...`);
            // https://github.com/wsbankenstein/terminal-bot/blob/main/storage/systems/748573404759589056.json/
            exec(`curl https://raw.githubusercontent.com/wsbankenstein/terminal-bot/main/storage/systems/${system}?token=GHSAT0AAAAAABURNPIXIIIFVB5ZNQYOJKACYUCTPOA > ./storage/systems/${system}`, (err, stdout, stderr) => {
                if (err) {
                    console.log(`Unable to clone dir: \n\n${err}`);
                    return;
                }
                console.log(`${system} has been cloned`);
            })
            stat += `${system}` + "\n";
        } else {
            console.log(`${system} is up to date`);
        }
    }
    return stat;
};


const diagnose = function () {
    return checkSystems();
}

module.exports = diagnose;