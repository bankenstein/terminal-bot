const fs = require('fs');
const Discord = require('discord.js');
const client = new Discord.Client({
	intents: new Discord.Intents(32767)
});
const config = require('./config.json');
const { exec, spawn } = require("child_process");
const { send } = require('process');
const http = require('https');
const request = require('request');
const diagnose = require('./check.js');

//load .env file and get the token
require('dotenv').config();
const token = process.env.TOKEN;

const defaultPrefix = '$ ';

function random(from, to) {
	return Math.floor(Math.random() * (to - from)) + from;
}

function numberToEnglish(n, custom_join_character) {

	var string = n.toString(),
		units, tens, scales, start, end, chunks, chunksLen, chunk, ints, i, word, words;

	var and = custom_join_character || 'and';

	/* Is number zero? */
	if (parseInt(string) === 0) {
		return 'zero';
	}

	/* Array of units as words */
	units = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];

	/* Array of tens as words */
	tens = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

	/* Array of scales as words */
	scales = ['', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion', 'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quatttuor-decillion', 'quindecillion', 'sexdecillion', 'septen-decillion', 'octodecillion', 'novemdecillion', 'vigintillion', 'centillion'];

	/* Split user arguemnt into 3 digit chunks from right to left */
	start = string.length;
	chunks = [];
	while (start > 0) {
		end = start;
		chunks.push(string.slice((start = Math.max(0, start - 3)), end));
	}

	/* Check if function has enough scale words to be able to stringify the user argument */
	chunksLen = chunks.length;
	if (chunksLen > scales.length) {
		return '';
	}

	/* Stringify each integer in each chunk */
	words = [];
	for (i = 0; i < chunksLen; i++) {

		chunk = parseInt(chunks[i]);

		if (chunk) {

			/* Split chunk into array of individual integers */
			ints = chunks[i].split('').reverse().map(parseFloat);

			/* If tens integer is 1, i.e. 10, then add 10 to units integer */
			if (ints[1] === 1) {
				ints[0] += 10;
			}

			/* Add scale word if chunk is not zero and array item exists */
			if ((word = scales[i])) {
				words.push(word);
			}

			/* Add unit word if array item exists */
			if ((word = units[ints[0]])) {
				words.push(word);
			}

			/* Add tens word if array item exists */
			if ((word = tens[ints[1]])) {
				words.push(word);
			}

			/* Add 'and' string after units or tens integer if: */
			if (ints[0] || ints[1]) {

				/* Chunk has a hundreds integer or chunk is the first of multiple chunks */
				if (ints[2] || !i && chunksLen) {
					words.push(and);
				}

			}

			/* Add hundreds word if array item exists */
			if ((word = units[ints[2]])) {
				words.push(word + ' hundred');
			}

		}

	}

	return words.reverse().join(' ');

}

class Dir {
	constructor(name, author) {
		this.name = name;
		this.type = 'dir';
		this.author = author;
		this.data = [];
	}
}

var golfCoin = 1;

class File {
	constructor(name, path, data, author) {
		this.name = name;
		this.path = path;
		this.data = data
		this.type = "file"
		this.info = {
			name: name,
			path: path,
			extension: path.split('.')[path.length - 1] ? path.split('.')[path.length - 1] : "None",
			size: this.data.length,
			lines: this.data.split('\n').length,
			date: new Date(),
			lastEdited: new Date(),
			author: author || 'Unknown'
		}
	}
	edit(data) {
		this.data = data;
		this.info.size = this.data.length;
		this.info.lines = this.data.split('\n').length;
		this.info.lastEdited = new Date();
	}
}

class FileSystem {
	constructor(ip) {
		this.data = [];
		this.prefix = "$ ";
		this.currentPath = "/";
		this.currentDir = this.data;
		this.pastDirs = [];
		this.load();
		this.output = "";
		this.ip = ip;
		this.connected = true;
		this.linked = "";
		this.password = "123456789";
		this.golf = 1;
		this.golfMine = 1;
		this.invite = "";
		this.version = "0.0.8";
		this.libs = []
	}
	save() {
		return fs.writeFileSync("./storage/systems/" + this.ip + ".json", JSON.stringify(this.data));
	}
	getFile(path) {
		//doesnt work :(
		let rpath = path.split('/');
		const fileName = rpath.pop();
		let file = this.getDir(rpath.join("/"));
		if (file == null && rpath.length > 0) return null;
		else if (file == null) file = this.currentDir || this.data;
		const out = file.filter(f => f.name == fileName && f.type == "file")[0]
		return out;
	}
	getDir(path) {
		let dir = this.currentDir;

		if (path == "/") {
			return this.data;
		}
		if (path.indexOf('/') == 0) {
			path = path.slice(1);
			dir = this.data;
		}
		let pathArray = path.split('/');
		for (let i = 0; i < pathArray.length; i++) {
			if (pathArray[i] == "..") {
				if (this.pastDirs.length == 0) return null;
				dir = this.pastDirs[this.pastDirs.length - 1];
			} else if (pathArray[i] == "~") {
				dir = this.data.find(d => d.name == "home").data;
			} else {
				dir = dir.find(f => f.name == pathArray[i] && f.type == "dir");
				if (dir) dir = dir.data;
				else return null;
			}
		}
		return dir;
	}

	load() {
		this.data.push(new Dir("bin", "root"));
		this.data[0].data.push(new File("help", "bin/help", "Commands are stored in /bin. Use 'cd' to navigate to the dir and use 'pwd' to see in which dir you are currently at.", "root"));
		this.data[0].data.push(new File("cd", "bin/cd", "navigate directories", "root"));
		this.data[0].data.push(new File("pwd", "bin/pwd", "show current path", "root"));
		this.data[0].data.push(new File("ls", "bin/ls", "list files and directories", "root"));
		this.data[0].data.push(new File("cat", "bin/cat", "displays the contents of a file", "root"));
		this.data[0].data.push(new File("touch", "bin/touch", "creates a empty file", "root"));
		this.data[0].data.push(new File("rm", "bin/rm", "removes a file", "root"));
		this.data[0].data.push(new File("mkdir", "bin/mkdir", "creates a directory", "root"));
		this.data[0].data.push(new File("rmdir", "bin/rmdir", "removes a directory", "root"));
		this.data[0].data.push(new File("clear", "bin/clear", "clears the terminal", "root"));
		this.data[0].data.push(new File("echo", "bin/echo", "displays a string", "root"));
		this.data[0].data.push(new File("exit", "bin/exit", "exits the terminal (goes to '/' and clears the terminal)", "root"));
		this.data[0].data.push(new File("log", "bin/log", "displays the log of the terminal", "root"));
		this.data.push(new Dir("home", "root"));
		this.data[1].data.push(new Dir("Desktop", "root"));
		this.data[1].data.push(new Dir("Documents", "root"));
		this.data[1].data.push(new Dir("Downloads", "root"));
		this.data[1].data.push(new Dir("Music", "root"));
		this.data[1].data.push(new Dir("Pictures", "root"));
		this.data[1].data.push(new Dir("Videos", "root"));
		this.currentDir = this.data;
		this.data.push(new Dir("records", "root"));
		this.data[2].data.push(new File("log", "records/log", "", "root"));
		this.data.push(new Dir("sys", "root"));
		this.data[3].data.push(new File("golf", "sys/golf", "", "root"));
		this.data[3].data.push(new File("trans", "sys/trans", "", "root"));
		this.data[3].data.push(new Dir("mines", "root"))
	}
}

function getGuilds() {
	let guilds = [];
	client.guilds.cache.forEach(g => {
		guilds.push({
			name: g.name,
			id: g.id,
			ownerId: g.ownerId,
			channels: g.channels.cache.map(c => { return { name: c.name, id: c.id } }),
			members: g.members.cache.map(m => { return { tag: m.user.tag, id: m.user.id } }),
			fileSystem: require("./storage/systems/" + g.id + '.json')
		});
		if (guilds[guilds.length - 1].fileSystem == {}) {
			guilds[guilds.length - 1].fileSystem = new FileSystem();
			fs.writeFileSync(`./storage/systems/${guilds[guilds.length - 1].id}.json`, JSON.stringify(guilds[guilds.length - 1].fileSystem, null, '\t'), (err) => {
				if (err) throw err;
				console.log("New guild file system was added!");
			});
		}
	});
	return guilds;
}

client.on('ready', async () => {
	await diagnose()
	console.log('Logged in as ' + client.user.tag);
	console.log(client.user.id)
	client.guilds.cache.map(g => {

		if (g.roles.cache.find(r => r.name == "Terminal") == undefined) {
			g.roles.create({
				name: "Terminal",
				color: "#0fe000",
				reason: "Terminal role"
			}).then(r => {
				console.log(`${r.name} with color: ${r.hexColor}, was added to ${g.name}`);

				g.members.cache.forEach(m => {
					if (m.client.id == client.user.id) {
						m.roles.add(r);
					}
				});
			});
		} else if (g.members.cache.find(m => m.user.id == client.user.id).roles.cache.find(r => r.name == "Terminal") == undefined) {
			g.members.cache.forEach(m => {
				if (m.client.id == client.user.id) {
					m.roles.add(g.roles.cache.find(r => r.name == "Terminal"));
				}
			});
		}
		console.log(g.name, g.memberCount);
		//get the save file data and fill it in a new class
		const data = require(`./storage/systems/${g.id}.json`);
		const guild = data;
		g.fileSystem = new FileSystem(guild.id);
		//rewrite the file system with the old data
		for (let i in guild) {
			g.fileSystem[i] = guild[i];
		}
		return;
		if (!g.channels.cache.find(c => c.name == "log") || !g.channels.cache.find(c => c.name == "log").permissionsFor(client.user).has("SEND_MESSAGES")) {//create one
			//and give permission only to the bot to send messages in that channel
			//create a channel called "log" and remove the permission to send messages in that channel
			g.channels.create("log", { type: "text" }).then(c => {
				c.permissionOverwrites.create(g.id, {
					SEND_MESSAGES: false
				});
				//give permission to the bot to send messages in that channel
				c.permissionOverwrites.create(client.user.id, {
					SEND_MESSAGES: true
				});
				//send the leaderboard of golfs
				let output = "";

				client.guilds.cache.sort((a, b) => {
					return b.memberCount - a.memberCount;
				})
					.forEach((guildSystem) => {
						output += `${output.split("\n").length}. [${guildSystem.name}](${guildSystem.fileSystem.invite}): ${guildSystem.fileSystem.golf}\n`;
						//only the first 10 guilds are displayed
						if (output.split("\n").length == 9) return;
					});
				//add the invite link to the bot
				output += `\n\n[Invite me](https://discordapp.com/api/oauth2/authorize?client_id=${client.user.id}&permissions=8&scope=bot)`;
				//send the message
				c.send({
					embeds: [
						{
							title: "Golf Leaderboard",
							description: output,
							color: 0x00ff00
						}
					]
				});
			})
		}
	});
	global.guilds = getGuilds();
	// fs.writeFileSync('./storage/guilds.json', JSON.stringify(guilds, null, '\t' /* TABS OVER SPACES! */));
});

//create a welcome msg with list of roles and he has to pick wich one to have by reaction
client.on('guildMemberAdd', member => {
	const name = ["welcome", "general", "main", "bot-commands"]
	member.guild.channels.cache.find(c => name.indexOf(c.name) != -1).send(`Welcome to the server, ${member}!`);
	member.guild.channels.cache.find(c => name.indexOf(c.name) != -1).send(`Please pick a role to have:`);
	member.guild.roles.cache.forEach(r => {
		member.guild.channels.cache.find(c => name.indexOf(c.name) != -1).send(`${r.name}`);
	});
	member.guild.channels.cache.find(c => name.indexOf(c.name) != -1).send(`No Role`)
	member.guild.channels.cache.find(c => name.indexOf(c.name) != -1).send(`Remove`)
	member.guild.channels.cache.find(c => name.indexOf(c.name) != -1).send(`Please react to the role you want to have! If you don't want to have any role, react to the role "No Role". If you want to be removed from the server, react to the role "Remove".`);
	member.guild.channels.cache.find(c => name.indexOf(c.name) != -1).awaitMessages(m => m.author.id == member.id, { max: 1, time: 30000, errors: ['time'] })
		.then(collected => {
			if (collected.first().content == "No Role") {
				member.guild.channels.cache.find(c => name.indexOf(c.name) != -1).send(`${member} has no role!`);
			} else if (collected.first().content == "Remove") {
				member.guild.channels.cache.find(c => name.indexOf(c.name) != -1).send(`${member} has been removed from the server!`);
				member.guild.members.cache.find(m => m.id == member.id).kick();
			} else {
				member.guild.channels.cache.find(c => name.indexOf(c.name) != -1).send(`${member} has been given the role ${collected.first().content}!`);
				member.guild.members.cache.find(m => m.id == member.id).roles.add(member.guild.roles.cache.find(r => r.name == collected.first().content));
			}
		}
		).catch(collected => {
			console.log(collected);
		});
})

function RNG(seed) {
	// LCG using GCC's constants
	this.m = 0x80000000; // 2**31;
	this.a = 1103515245;
	this.c = 12345;

	this.state = seed ? seed : Math.floor(Math.random() * (this.m - 1));
}
RNG.prototype.nextInt = function () {
	this.state = (this.a * this.state + this.c) % this.m;
	return this.state;
}
RNG.prototype.nextFloat = function () {
	// returns in range [0,1]
	return this.nextInt() / (this.m - 1);
}
RNG.prototype.nextRange = function (start, end) {
	// returns in range [start, end): including start, excluding end
	// can't modulu nextInt because of weak randomness in lower bits
	var rangeSize = end - start;
	var randomUnder1 = this.nextInt() / this.m;
	return start + Math.floor(randomUnder1 * rangeSize);
}
RNG.prototype.choice = function (array) {
	return array[this.nextRange(0, array.length)];
}

function mine() {
	const data = require('./storage/mines.json');
	for (let i in data) {
		let guild = require(`./storage/systems/${i}.json`);
		for (let n = 0; n < data[i].length; n++) {
			guild.golf += Number(data[i][n])
		}
		//edit the embeded message with title "Golf Leaderboard" in "log" channel with the new leaderboard
		let output = "";
		client.guilds.cache.sort((a, b) => {
			return b.memberCount - a.memberCount;
		})
			.forEach((guildSystem) => {
				output += `${output.split("\n").length}. [${guildSystem.name}](${guildSystem.fileSystem.invite}): ${guildSystem.fileSystem.golf}\n`;
				//only the first 10 guilds are displayed
				if (output.split("\n").length == 9) return;
			});
		//add the invite link to the bot
		output += `\n\n[Invite me](https://discordapp.com/api/oauth2/authorize?client_id=${client.user.id}&permissions=8&scope=bot)`;
		// client.guilds.cache.find(g => g.id == i).channels.cache.find(c => c.name == "log").messages.cache.find(m => m.embeds[0].title == "Golf Leaderboard").edit({
		// 	embed: {
		// 		title: "Golf Leaderboard",
		// 		description: output,
		// 		color: 0x00ff00
		// 	}
		// });
		//save
		fs.writeFileSync(`./storage/systems/${i}.json`, JSON.stringify(guild, null, '\t' /* TABS OVER SPACES! */));
	}
}
setInterval(mine, 10000);

function clean(amount, msg) {

	if (!amount) return msg.reply('You haven\'t given an amount of messages which should be deleted!');
	if (isNaN(amount)) return msg.reply('The amount parameter isn`t a number!');

	if (amount > 100) return msg.reply('You can`t delete more than 100 messages at once!');
	if (amount < 1) return msg.reply('You have to delete at least 1 message!');
	msg.channel.messages.fetch({ limit: amount }).then(messages => {
		msg.channel.bulkDelete(messages);
	});
}

//listen for messages
client.on('messageCreate', (msg) => {
	function send(m) {
		if (m.length > 2000) {
			m = m.substring(0, 2000 - ` and ${m.length - 2000} more characters...`.length) + ` and ${m.length - 2000} more characters...`;
		}
		return msg.channel.send(m);
	}
	var currentGuild = global.guilds.find(g => g.fileSystem.linked == msg.guild.id || g.id == msg.guild.id);
	var libs = []
	if (currentGuild && currentGuild.fileSystem.libs) {
		// load the js libs
		currentGuild.fileSystem.libs.forEach(name => {

			libs.push(require(`./storage/libs/${name}.js`))

		});
	}
	const prefix = currentGuild.fileSystem.prefix || defaultPrefix; if (msg.author.bot) return;
	if (msg.content.startsWith('code ```')) {
		let args = msg.content.replace("code ", "").split('```')[1];
		const r = args.substring(0, args.indexOf("\n")).toLowerCase();
		if (args.length > 0) args.slice(0, -3);
		else r = r.slice(0, -3);

		let name = `${random(10000, 99999)}`
		cmd = r;
		switch (cmd) {
			case "js" || "javascript":
				cmd = "js";
				break;
			case "py" || "python":
				cmd = "py";
				break;
			case "c++" || "cpp":
				cmd = "cpp";
				break;
			case "c":
				cmd = "c";
				break;
			case "bash" || "shell" || "sh":
				cmd = "sh";
				break;
			default:
				return;

		}
		exec(`cp -r ./storage/programs/${cmd} ./storage/fields/${name}`, (error, stdout, stderr) => {
			if (error) return send(`\`\`\`Error: ${error.message}\`\`\``);
			if (!stderr) {
				fs.writeFile(`./storage/fields/${name}/main.${cmd}`, args.replace(r, ""), (err) => {
					if (err) return send(`\`\`\`Error: ${err.message}\`\`\``);
					console.log("File created in: " + `./storage/fields/${name}/main.${cmd}`);
					send("Creating an environment for your program...");
					if (cmd == "cpp") {
						exec(`g++ -o ./storage/fields/${name}/main ./storage/fields/${name}/main.cpp`, (error, stdout, stderr) => {
							if (error) console.log(error.message)
							if (stderr) return send(`\`\`\`${stderr}\`\`\``)
							if (stdout) console.log("./main built")
							exec(`sudo docker build -t ${name}:latest ./storage/fields/${name}`, (error, stdout, stderr) => {
								if (error || stderr) {
									send("Couldn't build the environment!");
									console.log(error.message, stderr);
								} else {
									console.log("Enviorment built!");
									send("Environment built!");
								}
								exec(`sudo sh ./storage/fields/run.sh ${name}`, (err, std, serr) => {
									if (err) send(`\`\`\`Error: ${err.message}\`\`\``);
									if (serr) send(`\`\`\`Stderr: ${serr}\`\`\``);
									if (std) send(`\`\`\`Stdout: ${std}\`\`\``);
									else send("```No stdout!```");
									exec(`sudo docker rmi -f $(sudo docker images '${name}:latest' -a -q); rm -rvf ./storage/fields/${name}`, (error, stdout, stderr) => {
										if (error) console.log(`Error killing proccess ${name}: ` + error.message);
										else if (stderr) console.log(`Error killing proccess ${name}: ` + stderr.message);
										else console.log(`Killed proccess ${name}`);
									})
								})
							})
						})
					} else {
						exec(`sudo docker build -t ${name}:latest ./storage/fields/${name}`, (error, stdout, stderr) => {
							if (error || stderr) {
								send("Couldn't build the enviorment!");
								console.log(error.message, stderr);
							} else {
								console.log("Enviorment built!");
								send("Enviorment built!");
							}
							exec(`sudo sh ./storage/fields/run.sh ${name}`, (err, std, serr) => {
								if (err) send(`\`\`\`Error: ${err.message}\`\`\``);
								if (serr) send(`\`\`\`Stderr: ${serr}\`\`\``);
								if (std) send(`\`\`\`Stdout: ${std}\`\`\``);
								else send("```No stdout!```");
								exec(`sudo docker rmi -f $(sudo docker images '${name}:latest' -a -q); rm -rvf ./storage/fields/${name}`, (error, stdout, stderr) => {
									if (error) console.log(`Error killing proccess ${name}: ` + error.message);
									else if (stderr) console.log(`Error killing proccess ${name}: ` + stderr.message);
									else console.log(`Killed proccess ${name}`);
								})
							})
						})
					}
				})
			}
		})
		return;
	}
	if ((msg.content.startsWith('`' + prefix) && msg.content.charAt(msg.content.length - 1) == '`') || msg.content.startsWith(prefix)) {
		let args = [], cmd = "";
		if (msg.content.startsWith('`' + prefix) && msg.content.charAt(msg.content.length - 1)) {
			args = msg.content.slice(prefix.length + 1).split(' ');
			cmd = args.shift().toLowerCase();

			if (args.length > 0) args[args.length - 1] = args[args.length - 1].slice(0, -1);
			else cmd = cmd.slice(0, -1);
		} else {
			args = msg.content.slice(prefix.length).split(' ');
			cmd = args.shift().toLowerCase();
		}
		if (cmd == "sudo" && (msg.member.permissions.has(["ADMINISTRATOR"]) || config.owner.indexOf(msg.author.id) != -1 || msg.channel.type == "DM")) {
			cmd = args.shift();
			var sudo = true;
		}

		console.log(`${msg.author.tag} (${msg.author.id}) ran command: "${cmd}" with args: ${JSON.stringify(args)}`);
		currentGuild.fileSystem.output += `${msg.author.tag}${msg.content.replace(/`/g, "")}\n`;

		if (cmd.startsWith("ps1")) {
			let m = ""
			if (msg.content.startsWith('`') && msg.content.charAt(msg.content.length - 1) == '`') {
				m = msg.content.replace(/`/g, "").toLowerCase();
			} else m = msg.content.toLowerCase();
			const p = m.replace(currentGuild.fileSystem.prefix + "ps1", "");
			if (p.length == 0) return send(`Current PS1 is: "${currentGuild.fileSystem.prefix}"`);
			currentGuild.fileSystem.prefix = p.slice(1).replace(/"|'/g, "");
			send("PS1 set to: `" + currentGuild.fileSystem.prefix + "`");
		}

		if (libs.length) {
			libs.forEach(lib => {
				if (cmd == lib.usage) {
					lib.execute(msg, args, sudo)
				}
			})
		}

		if (cmd == "ascii") {
			let avatar = msg.author.avatarURL({ format: "png", dynamic: true, size: 1024 });
			if (args.length > 0) {
				// args[0] will be a mention. Get the avatar from the mentioned user
				avatar = msg.guild.members.cache.find(m => m.id == args[0].replace(/[<@!>]/g, "")).user.avatarURL({ format: "png", dynamic: true, size: 1024 });
			}
			// get the author's avatar, save it to a png formatted file in ./storage/ascii/ and execute command "asciiart ./storage/ascii/avatar.png > ./storage/ascii/ascii.txt". Afer that send ascii.txt as a file.
			console.log(JSON.stringify(avatar))
			const file = fs.createWriteStream(`./storage/ascii/${msg.author.id}.png`);
			const request = http.get(avatar, function (response) {
				response.pipe(file);
				file.on('finish', function () {
					file.close();
					exec(`asciiart ./storage/ascii/${msg.author.id}.png > ./storage/ascii/${msg.author.id}.txt`, (error, stdout, stderr) => {
						if (error) send(`\`\`\`Error: ${error.message}\`\`\``);
						if (stderr) send(`\`\`\`Stderr: ${stderr}\`\`\``);
						if (stdout) send(`\`\`\`Stdout: ${stdout}\`\`\``);
						exec(`rm ./storage/ascii/${msg.author.id}.png`, (error, stdout, stderr) => {
							if (error) console.log(`Error removing avatar.png: ` + error.message);
							else if (stderr) console.log(`Error removing avatar.png: ` + stderr.message);
							else console.log(`Removed avatar.png`);
						})
						var out = fs.readFileSync(`./storage/ascii/${msg.author.id}.txt`, "utf8");
						out = out.split("\n")
						out.shift()
						// write out to the file
						fs.writeFile(`./storage/ascii/${msg.author.id}.txt`, out.join("\n"), function (err) {
							// send out as a file attachment
							msg.channel.send({
								files: [{
									attachment: `./storage/ascii/${msg.author.id}.txt`,
									name: `${msg.author.tag}.txt`
								}]
							})
							// remove the file
							setTimeout(() => {
								exec(`rm ./storage/ascii/${msg.author.id}.txt`, (error, stdout, stderr) => {
									if (error) console.log(`Error removing ascii.txt: ` + error.message);
									else if (stderr) console.log(`Error removing ascii.txt: ` + stderr.message);
									else console.log(`Removed ascii.txt`);
								})
							}, 1000)
						})
					});
				})
			})
		}

		if (cmd == "ping") {
			//give the user, the bots ping
			send(`Pong! \n__${msg.createdTimestamp - msg.createdTimestamp}__ms message delay. \nBot latency: __${Math.round(client.ws.ping)}__ms`);
		}
		if (cmd == "apt") {
			if (args.length == 0) return send("apt requires an argument!");
			switch (args[0]) {
				case "install":
					if (!sudo) return;
					if (args.length < 2) return send("apt install requires an argument!");
					const lib = args[1];
					const libs = fs.readdirSync("./storage/libs");
					if (libs.indexOf(lib + ".js") == -1) return send("Couldn't find the library!");
					if (currentGuild.fileSystem.libs.indexOf(lib) != -1) return send("The library is already installed!");
					currentGuild.fileSystem.libs.push(lib);
					send(`Installed ${lib}!`);
					break;
				case "search":
					if (args.length < 2) return send("apt search requires an argument!");
					const search = args[1];
					const l = fs.readdirSync("./storage/libs");
					let out = ""
					l.filter(s => s.toLowerCase().indexOf(search.toLowerCase() + ".js") != -1).forEach(s => {
						const info = require("./storage/libs/" + s);
						out += `__${s}__ - ${info.description}\n`;
					});
					send(out);
					break;
				case "remove":
					if (!sudo) return;
					if (args.length < 2) return send("apt remove requires an argument!");
					const remove = args[1];
					const libraries = currentGuild.fileSystem.libs;
					if (libraries.indexOf(remove) == -1) return send("Couldn't find the library!");
					libraries.splice(libraries.indexOf(remove), 1);
					send("Done!")
					break;
				case "list":
					const libs2 = currentGuild.fileSystem.libs;
					let out2 = "";
					libs.forEach(lib => {
						out2 += `__${lib}__\n`;
					})
					send(out2);
					break;
			}
		}

		if (cmd == "golf") {
			switch (args[0]) {
				case "--show" || "-s":
					send(`\`\`\`Current guild has ${currentGuild.fileSystem.golf.toFixed(5)} golfs. \nMine price is: ${currentGuild.fileSystem.golfMine} golfs.\`\`\``);
					break;
				case "-am" || "--add":
					if (isNaN(args[1])) return send("Please specify a number!");
					//create a file for every mine named `mine(${i})` and set value to be 0.001
					if (currentGuild.fileSystem.golf < currentGuild.fileSystem.golfMine * parseInt(args[1])) return send("You don't have enough golfs to buy a mine!");
					currentGuild.fileSystem.golf -= currentGuild.fileSystem.golfMine * parseInt(args[1])
					let files = currentGuild.fileSystem.getDir("/sys/mines").length;
					const l = args.length > 1 ? parseInt(args[1]) : 1
					let dir = currentGuild.fileSystem.getDir("/sys/mines");

					if (!dir) return send("`/sys/mines` not found!");
					let d = require(`./storage/mines.json`);
					if (l > 100) {
						d[currentGuild.id].push(0.001 * l + "")
						return send("You have added " + l + " mines!");
					}
					for (let i = 0; i < l; i++) {
						//create a file for every mine named `mine(${i})` and set value to be 0.001
						dir.push(new File(`mine(${files + i})`, `/sys/mines`, "0.001", "root"));
						if (d[currentGuild.id] == undefined) d[currentGuild.id] = [];
						d[currentGuild.id].push("0.001")

						//create a function in __proto__ that counts how many times does an element appear in an array
						Array.prototype.count = function (element) {
							return this.filter(e => e == element).length;
						}
						currentGuild.fileSystem.golfMine = currentGuild.fileSystem.golfMine * d[currentGuild.id].length;
					}

					for (let i = 1; i < d[currentGuild.id].length - 1; i++) {
						//sum all the values in the array but first make them numbers
						d[currentGuild.id][0] = (Number(d[currentGuild.id][0]) + Number(i)).toString();
					}
					d[currentGuild.id] = [d[currentGuild.id][0]];
					fs.writeFileSync(`./storage/mines.json`, JSON.stringify(d));
					send(`Added ${args[1] ? Number(args[1]) : 1} mines`);
					break;
				case "--upgrade" || "-u":
					if (args.length < 2) return send("Please specify the index of the mine(s) to upgrade!");
					for (let i = 0; i < args.length - 1; i++) {
						const index = Number(args[i + 1]);
						if (index == NaN) return send(`Invalid parameter! There is no mine with index ${args[i + 1]}!`);
						let file = currentGuild.fileSystem.getFile(`/sys/mines/mine(${index})`);
						file.data = (Number(file.data) + 0.001).toString();
						d[currentGuild.id][index] = file.data;
						send(`\`[*] Mine ${index} upgraded!\``);
					}
					fs.writeFileSync(`./storage/mines.json`, JSON.stringify(d));
					break;
				case "--mines" || "-m":
					const mines = currentGuild.fileSystem.getDir(`/sys/mines`);
					let minesList = "";
					for (let i = 0; i < mines.data.length; i++) {
						minesList += `${mines.data[i].name} - ${mines.data[i].data}\n`;
					}
					msg.reply(`\`\`\`${minesList.length > 0 ? minesList : "There are no mines created. You can create one with `golf -am`. Limit of mines are 10. if there are more than 10, some of the mines will become offline."}\`\`\``);
					break;
				case "-t" || "--transfer":
					if (args.length < 3) return send("Not enough arguments!");
					const ip = args[1];
					const amount = Number(args[2]);
					if (isNaN(amount)) return send("Please specify a number for the second argument!");
					let guildSystem = global.guilds.find(g => g.id == ip);
					if (!guildSystem) return send("Invalid guild!");
					guildSystem.fileSystem.golf += amount;
					currentGuild.fileSystem.golf -= amount;
					//save the new amount of golfs
					fs.writeFileSync(`./storage/systems/${currentGuild.id}`, JSON.stringify(currentGuild.fileSystem, null, 4));
					fs.writeFileSync(`./storage/systems/${guildSystem.id}`, JSON.stringify(guildSystem.fileSystem, null, 4));
					send(`Transfered ${amount} golfs to ${guildSystem.name}`);
					break;
				case '-l' || "--leaderboard":
					let guilds = global.guilds;
					let guildsList = "";
					//sort from largest to smallest
					guilds.sort((a, b) => {
						return b.fileSystem.golf - a.fileSystem.golf;
					});
					for (let i = 0; i < guilds.length; i++) {
						//hyperlink with the invite link
						if (guilds[i].fileSystem.invite == undefined) guildsList += `${i + 1}. __${guilds[i].name}__ - ${guilds[i].fileSystem.golf}\n`;
						else guildsList += `${i + 1}. [${guilds[i].name}](${guilds[i].fileSystem.invite}) - ${guilds[i].fileSystem.golf}\n`;
					}
					//send an embed
					send({
						embeds: [
							{
								title: "Golf Leaderboard",
								description: guildsList.length > 0 ? guildsList : "There are no guilds with golf!",
								color: 0x00ff00
							}
						]
					})
					break;
				default:
					msg.reply("Invalid command! Use `golf -s` to see the current amount of golfs in this guild.");
					break;
			}
		}

		if (cmd == "cp") {
			if (args.length < 2) {
				send("Please specify a source and a destination!");
				return;
			}
			let sourceFile = currentGuild.fileSystem.getFile(args[0]);
			let destinationFile = currentGuild.fileSystem.getFile(args[1]);
			if (sourceFile == null) {
				send("Source file not found!");
				return;

			}
			if (destinationFile == null) {
				const path = args[1].split('/');
				const name = path.pop();
				destinationFile = new File(name, (path.join("/").length == 0 ? currentGuild.fileSystem.currentPath : path.join("/")), sourceFile.data)
				if (currentGuild.fileSystem.getDir(path.join("/")) == null) currentGuild.fileSystem.currentDir.push(destinationFile);
				else currentGuild.fileSystem.getDir(path.join("/")).push(destinationFile);
			} else {
				if (sourceFile.type == "dir" || destinationFile.type == "dir") {
					send("You cannot move a directory to a file!");
					return;
				}
				destinationFile.edit(sourceFile.data);
			}

			send("File copied!");
		}
		if (cmd == "finfo") {
			if (args.length < 1) {
				send("Please specify a file!");
				return;
			}
			let file = currentGuild.fileSystem.getFile(args[0]);
			if (file == null) {
				send("File not found!");
				return;
			}
			var out = "";
			for (let i in file.info) out += `${i}: ${file.info[i]}\n`;
			send(out);
		}

		if (cmd == "mv") {
			if (args.length < 2) {
				send("Please specify a source and a destination!");
				return;
			}
			let sourceFile = currentGuild.fileSystem.getFile(args[0]);
			let destinationFile = currentGuild.fileSystem.getFile(args[1]);
			if (sourceFile == null) {
				send("Source file not found!");
				return;
			}
			if (sourceFile.info.author != msg.author.id && !sudo) {
				send("You cannot move a file that you don't own!");
				return;
			}

			if (destinationFile == null) {
				const path = args[1].split('/');
				const name = path.pop();
				destinationFile = new File(name, (path.join("/").length == 0 ? currentGuild.fileSystem.currentPath : path.join("/")), sourceFile.data)
				if (currentGuild.fileSystem.getDir(path.join("/")) == null) currentGuild.fileSystem.currentDir.push(destinationFile);
				else currentGuild.fileSystem.getDir(path.join("/")).push(destinationFile);
			} else {
				if (sourceFile.type == "dir" || destinationFile.type == "dir") {
					send("You cannot move a directory to a file!");
					return;
				}
				destinationFile.edit(sourceFile.data);
			}
			console.log(sourceFile.path)
			const p = sourceFile.path || currentGuild.fileSystem.currentPath;
			currentGuild.fileSystem.getDir(p).splice(currentGuild.fileSystem.getDir(p).indexOf(sourceFile), 1);

			send("File moved!");
		}

		if (cmd == "ssh") {
			if (args.length < 1) {
				send("Please specify ip and password!");
				return;
			}
			const guild = global.guilds.find(g => g.fileSystem.ip == args[0]);
			if (guild.fileSystem.password == args[1]) {
				guild.fileSystem.linked = currentGuild.fileSystem.ip;
				send(`SSH connected to ${args[0]}! You can not remove files from this server!`);
			}
		}

		if (cmd == "restart" && config.owner.indexOf(msg.author.id) != -1) {
			if (sudo) {
				currentGuild.fileSystem = new FileSystem(msg.guild.id);
				fs.writeFileSync(`./storage/systems/${currentGuild.id}.json`, JSON.stringify(currentGuild.fileSystem, null, '\t'), (err) => {
					if (err) throw err;
					console.log("New guild file system was added!");
				})
			} else {
				//get the save file data and fill it in a new class
				const data = fs.readFileSync(`./storage/systems/${currentGuild.id}.json`);
				const guild = JSON.parse(data);
				currentGuild.fileSystem = new FileSystem(guild.id);
				//rewrite the file system with the old data
				for (let i in guild) {
					currentGuild.fileSystem[i] = guild[i];
					console.log(`Updating: ${i}`)
				}
				fs.writeFile(`./storage/systems/${currentGuild.id}.json`, JSON.stringify(currentGuild.fileSystem, null, '\t'), (err) => {
					if (err) throw err;
					console.log("Guild restarted!");
				})
			}
		}

		if (cmd == "neofetch") {
			const owner = currentGuild.members.find(u => u.id == msg.guild.ownerId).tag;
			let embed = new Discord.MessageEmbed()
				.setTitle("root@Terminal-bot")
				.setColor(0x00ff00)
				.setDescription("**OS**: DiscordOS \n[Invite me!](https://discord.com/oauth2/authorize?client_id=926439488090148895&scope=bot&permissions=8)")
				.addField("**Kernel**:", "Linux")
				.addField("**Uptime**:", "0 days, 0 hours, 0 minutes, 0 seconds")
				.addField("**Hostname**:", "Terminal-bot")
				.addFields({
					name: "Guild info",
					value: `**Guild name:** ${msg.guild.name}\n**Guild IP:** ${msg.guild.id}\n**Guild owner:** ${owner ? owner : "__Error loading owner info__"}\n**Guild created at:** ${msg.guild.createdAt}\n**Guild member count:** ${msg.guild.memberCount}`,
					inline: true
				},
					{
						name: "Guild's file system info",
						value: `**prefix**: ${prefix}\n**Current path:** ${currentGuild.fileSystem.currentPath}\n**Current directory file length:** ${currentGuild.fileSystem.currentDir.length}\n**version**: ${currentGuild.fileSystem.version}`/***Total files:** ${currentGuild.fileSystem.totalFiles}\n**Total directories:** ${currentGuild.fileSystem.totalDirs}`*/,
						inline: true
					})
				.setFooter({
					text: "Can we reach 100 servers?",
					iconURL: msg.author.avatarURL
				})
				.setTimestamp()
				.setThumbnail("https://musician.lenwe.io/import/img/discord-icon.png");
			send({ embeds: [embed] });
		}

		if (cmd == 'echo') {
			send("```" + (args.join(' ').length == 0 ? "Please enter a message to echo." : args.join(' ')) + "```");
			currentGuild.fileSystem.output += args.join(" ") + "\n";
		}

		if (cmd == "cd") {
			if (args.length < 1) return send("CD is short from 'change directory'. Use `cd [directoryPath]` to change directories.");
			const dir = currentGuild.fileSystem.getDir(args.join(" "));
			if (!dir) return send("Directory not found!");
			const p = args.join(" ").split("/")
			for (let i = 0; i < p.length; i++) {
				if (p[i] == "..") {
					if (currentGuild.fileSystem.currentPath.split("/").length == 1) {
						return send("You can not go back from root directory!");
					}
					currentGuild.fileSystem.currentPath = currentGuild.fileSystem.currentPath.split("/").slice(0, -1).join("/");
				} else if (p[i] == ".") {
					continue;
				} else if (p[i] == "~") {
					currentGuild.fileSystem.currentPath = "/home";
				} else if (p[i] == "") {
					currentGuild.fileSystem.currentPath = p.join("/");

					currentGuild.fileSystem.currentDir = dir;
					send(`Current path changed to ${args.join(" ")}!`);
					break;
				} else {
					currentGuild.fileSystem.currentPath += "/" + p[i];
				}
			}
			currentGuild.fileSystem.pastDirs.push(currentGuild.fileSystem.currentDir);
			currentGuild.fileSystem.currentDir = dir;
			send(`Current path changed to ${args.join(" ")}!`);
			// if (args[0].indexOf("/") == 0) {
			// 	currentGuild.fileSystem.currentDir = currentGuild.fileSystem.data;
			// 	currentGuild.fileSystem.pastDirs = [];
			// 	currentGuild.fileSystem.currentPath = "/";
			// }
			// for (let i = 0; i < params.length; i++) {
			// 	let dir = currentGuild.fileSystem.currentDir.find(d => d.name == params[i]);
			// 	if (dir && dir.type == "dir") {
			// 		currentGuild.fileSystem.pastDirs.push(currentGuild.fileSystem.currentDir);
			// 		currentGuild.fileSystem.currentDir = dir.data;
			// 		currentGuild.fileSystem.currentPath += currentGuild.fileSystem.currentPath != "/" ? "/" + dir.name : dir.name;
			// 		send(currentGuild.fileSystem.currentPath);
			// 	} else if (params[i] == "..") {
			// 		if (currentGuild.fileSystem.pastDirs.length == 0) {
			// 			send("You cannot go back further than the root directory.");
			// 			break;
			// 		}
			// 		currentGuild.fileSystem.currentPath = currentGuild.fileSystem.currentPath.slice(0, currentGuild.fileSystem.currentPath.lastIndexOf("/"));
			// 		if (currentGuild.fileSystem.currentPath.length == 0) currentGuild.fileSystem.currentPath = "/";
			// 		currentGuild.fileSystem.currentDir = currentGuild.fileSystem.pastDirs.pop();
			// 		send(currentGuild.fileSystem.currentPath);
			// 	} else if (params[i].length != 0) {
			// 		currentGuild.fileSystem.output += "Directory not found" + "\n";
			// 		send("Directory not found");
			// 	}
			// }
		}

		if (cmd == "pwd") {
			currentGuild.fileSystem.output += currentGuild.fileSystem.currentPath + "\n";
			send(currentGuild.fileSystem.currentPath);
		}

		if (cmd == "dc") {
			if (args.length < 1) return send("Please specify a seed and a message to encode!");
			//create dirty code
			let seed = args.shift(), text = "";
			if (args[args.length - 1] == "--decode") {
				text = args.join(" ").slice(0, -9);
			} else text = args.join(" ");
			let o = ""

			//set the seed of random to be the seed
			console.log(String.raw`${text}`)
			let rng = new RNG(seed);
			for (let i = 0; i < text.length; i++) {
				o += String.fromCharCode(text.charCodeAt(i) ^ rng.nextRange(0, 256));
			}
			send("Seed: " + seed + "```" + o + "```");
		}

		if (cmd == "ls") {
			const path = args.length == 0 ? currentGuild.fileSystem.currentPath : args[0];
			const dir = currentGuild.fileSystem.getDir(path);
			if (dir) {
				const files = dir.filter(f => f.type == "file");
				const dirs = dir.filter(f => f.type == "dir");
				let output = "";
				for (let i = 0; i < files.length; i++) {
					output += files[i].name + "\n";
				}
				for (let i = 0; i < dirs.length; i++) {
					output += `**${dirs[i].name}/**\n`;
				}
				currentGuild.fileSystem.output += output + "\n";
				send(output.length == 0 ? "No files or directories found." : output);
			} else {
				currentGuild.fileSystem.output += "Invalid path" + "\n";
				send("Invalid path");
			}
		}

		if (cmd == "touch") {
			if (args.length == 0) return send("Touch creates a file. Use `" + prefix + "touch [filename]` to create a file.");
			let dir = args[0].indexOf("/") != -1 ? currentGuild.fileSystem.getDir(args[0].substring(0, args[0].lastIndexOf("/"))) : currentGuild.fileSystem.currentDir;
			if (dir == null) return send("Directory not found");
			if (args[0].split("/").pop().match(/[\/\\]/g)) return send("Invalid file name");
			let file = new File(args[0].split("/").pop(), currentGuild.fileSystem.currentPath + args[0], "", msg.author.id);
			if (dir.indexOf(file) == -1) dir.push(file);
			else send("File already exists");
			currentGuild.fileSystem.output += "Created file " + args[0] + "\n";
			send("File created");
		}

		if (cmd == "rm") {
			if (args.length == 0) return send("rm deletes a file. Use `" + prefix + "rm [filename]` to delete a file.");
			let file = currentGuild.fileSystem.getFile(args[0]);
			if (file) {
				if (file.author == msg.author.id || sudo) {
					currentGuild.fileSystem.currentDir.splice(currentGuild.fileSystem.data.indexOf(file), 1);
					currentGuild.fileSystem.output += "File removed" + "\n";
					send("File removed");
				} else {
					send("You do not have permission to remove this file.");
				}
			} else {
				currentGuild.fileSystem.output += "File not found" + "\n";
				send("File not found");
			}
		}

		if (cmd == "mkdir") {
			if (args.length == 0) return send("mkdir creates a directory. Use `" + prefix + "mkdir [directory name]` to create a directory.");
			let d = currentGuild.fileSystem.currentDir;
			if (args[0].indexOf("/") != -1) {
				d = currentGuild.fileSystem.getDir(args[0].substring(0, args[0].lastIndexOf("/")));
				console.log(args[0].substring(0, args[0].lastIndexOf("/")), "dir: " + d);
			}
			let dir = new Dir(args[0].split("/").pop(), currentGuild.fileSystem.currentPath + args[0]);
			if (d.indexOf(dir) == -1) d.push(dir);
			currentGuild.fileSystem.output += "Directory created" + "\n";
			send("Directory created");
		}

		if (cmd == "rmdir") {
			let dir = currentGuild.fileSystem.currentDir.find(d => d.name == args[0] && d.type == "dir");
			if (dir) {
				if (dir.author == msg.author.id || sudo) {
					currentGuild.fileSystem.currentDir.splice(currentGuild.fileSystem.currentDir.indexOf(dir), 1);
					currentGuild.fileSystem.output += "Directory removed" + "\n";
					send("Directory removed");
				} else {
					send("You do not have permission to remove this directory.");
				}
			} else {
				currentGuild.fileSystem.output += "Direcotry not found" + "\n";
				send("Directory not found");
			}
		}

		if (cmd == "cat") {
			if (args.length == 0) {
				send("No file specified")
			} else {
				const dirs = args[0].indexOf("/") == 0 ? args[0].slice(1).split('/') : args[0].split('/');
				if (args[0].indexOf("/") == 0) {
					currentGuild.fileSystem.currentDir = currentGuild.fileSystem.data;
					currentGuild.fileSystem.pastDirs = [];
					currentGuild.fileSystem.currentPath = "/";
				}
				for (let i = 0; i < dirs.length - 1; i++) {
					if (dirs[i] != "..") {
						let dir = currentGuild.fileSystem.currentDir.find(d => d.name == dirs[i] && d.type == "dir");
						if (dir) {
							currentGuild.fileSystem.pastDirs.push(currentGuild.fileSystem.currentDir);
							currentGuild.fileSystem.currentDir = dir.data;
						} else {
							console.log("Dir not found.")
							break;
						}
					} else {
						if (currentGuild.fileSystem.pastDirs.length == 0) {
							console.log("No past dirs.")
							break;
						}
						currentGuild.fileSystem.currentDir = currentGuild.fileSystem.pastDirs.pop();
					}
				}
				let file = currentGuild.fileSystem.currentDir.find(f => f.name == dirs[dirs.length - 1] && f.type == "file");
				if (file) {
					currentGuild.fileSystem.output += file.data + "\n";
					send(file.data.length == 0 ? "File is empty" : file.data);
				} else {
					currentGuild.fileSystem.output += "File not found" + "\n";
					send("File not found");
				}
				for (let n = 0; n < dirs.length - 1; n++) {
					currentGuild.fileSystem.currentDir = currentGuild.fileSystem.pastDirs.pop();
				}
			}
		}

		if (cmd == "clear") {
			currentGuild.fileSystem.output = "";
			send("Log cleared");
			if (args.length != 0) clean(args[0], msg)

		}

		if (cmd == "help") {
			currentGuild.fileSystem.output += "Commands are stored in /bin" + "\n";
			if (args.length == 0) {
				//create a list wich ls-s /bin
				let list = [],
					dir = currentGuild.fileSystem.getDir("/bin");
				if (!dir) {
					return;
				}
				for (let i = 0; i < dir.length; i++) {
					if (dir[i].type == "file") {
						list.push(`${dir[i].name} : \`\`\`${dir[i].data}\`\`\``);
					}
				}
				return send(`PS1 of the terminal is \`${prefix}\`. The syntax is: \`${prefix}[command] [arguments]\`\n\n This bot is based on the linux terminal and is currently in beta.\n\nCommands:\n\n${list.join("\n\n")}\n\nUse \`${prefix}help [command]\` to get more information about a command.`);
			}
			const f = currentGuild.fileSystem.data[0].data.filter(d => d.name == args[0] && d.type == "file")[0]
			if (f) {
				currentGuild.fileSystem.output += f.data + "\n";
				send(f.data);
			} else {
				send("```__Error__: /bin/" + args[0] + " not found: no such direcotry or filename```");
			}
		}

		if (cmd == "exit") {
			if (currentGuild.id != msg.guild.id) currentGuild.fileSystem.linked = "";
			currentGuild.fileSystem.output += "Exited" + "\n";
			currentGuild.fileSystem.currentPath = "/";
			currentGuild.fileSystem.output = "";
			send("Exited");
		}

		if (cmd == "") {
			currentGuild.fileSystem.output += "Command not found" + "\n";
			send("Command not found");
		}

		if (cmd == "log") {
			currentGuild.fileSystem.output += " log \n";
			if (args[0] == "clear") {
				currentGuild.fileSystem.output = "";
				send("Log cleared");
			} else if (args[0] == "save") {
				currentGuild.fileSystem.data[1].data[0].data.push(new File(new Date().toString().replace(/ +/g, "_") + ".txt", "/home/Desktop", currentGuild.fileSystem.output, "root"));
				currentGuild.fileSystem.output += "Log saved on Desktop" + "\n";
			} else {
				if (currentGuild.fileSystem.output.length > 2000) return send("Log is too long to send. Use `" + prefix + "log clear` or `" + prefix + "clear` to clear the log.");
				send(`\`\`\`${currentGuild.fileSystem.output}\`\`\``);
			}
		}
		if (cmd == "record") {
			if (args.length == 0) {
				send("Record is a report command. Use `record -e [mention] [reportMessage]` to report a person. Use `record -l` to list reports and `record -d [mention]` to delete a record (sudo only).");
			}
			if (args[0] == "-show" || args[0] == "-s") {
				let record = currentGuild.fileSystem.data[2].data.find(r => r.name == args[1]);
				if (record) {
					send(`\`\`\`${record.data}\`\`\``);
				} else {
					send("Record not found");
				}
			} else if ((args[0] == "-delete" || args[0] == "-d") && sudo) {
				let record = currentGuild.fileSystem.data[2].data.find(r => r.name == args[1]);
				if (record) {
					currentGuild.fileSystem.data[2].data.splice(currentGuild.fileSystem.data[2].data.indexOf(record), 1);
					//add the action to log
					currentGuild.fileSystem.data[2].data.map(r => r.name == "log" ? r.data += `${msg.author.username} deleted record ${args[1]}\n` : r);
					send("Record deleted");
				} else {
					send("Record not found");
				}
			} else if (args[0] == "-list" || args[0] == "-l") {
				let records = currentGuild.fileSystem.data[2].data.slice(1).map(r => r.name);
				send("List of records: \n" + records.join(", "));
			} else if (args[0] == "-edit" || args[0] == "-e") {
				let record = currentGuild.fileSystem.data[2].data.find(r => r.name == args[1]);
				if (record) {
					record.data += "\n" + args.slice(2).join(" ");
				} else {
					//check if args[1] is a mention
					if (args[1].startsWith("<@") && args[1].endsWith(">")) currentGuild.fileSystem.data[2].data.push(new File(args[1], "/records", args.slice(2).join(" ")));
					else return send("You must mention a user to record to as the first argument");
				}
				currentGuild.fileSystem.data[2].data.map(r => r.name == "log" ? r.data += `${msg.author.username} edited record "${args[1]}"\n` : r);
				send("Record edited");
			}
		}
		if (cmd == "invite") {
			msg.channel.createInvite({
				maxAge: 0,
				maxUses: 0,
				temporary: false,
				unique: false,
				reason: "Invite command"
			}).then(invite => {
				currentGuild.fileSystem.output += "Invite created: " + invite.url + "\n";
				currentGuild.fileSystem.invite = invite.url;
				send(`Invite: ${invite.url}`);
			});
		}

		if (cmd == "nano") {
			let edited = false;
			let dir = currentGuild.fileSystem.currentDir;
			if (args[0].indexOf("/") != -1) {
				let path = args[0].split("/");
				path.pop()
				dir = currentGuild.fileSystem.getDir(path.join("/"));
			}

			dir.map(f => {
				if (f.name == args[0] && f.type == "file" && (f.author == msg.author.id || sudo)) {
					f.data = args.slice(1).join(" ");
					edited = true;
				}
			});
			if (!edited) {
				//create file if its not there
				if (dir.author != "root" && !sudo) {
					dir.push(new File(args[0], currentGuild.fileSystem.currentPath + args[0], args.slice(1).join(" ")));
				} else if (sudo) {
					dir.push(new File(args[0], currentGuild.fileSystem.currentPath + args[0], args.slice(1).join(" ")));
				} else {
					send("You cannot create files in this directory");
				}
			}
			currentGuild.fileSystem.output += "File edited" + "\n";
			send("File edited");
		}
		fs.writeFile("./storage/systems/" + currentGuild.id + ".json", JSON.stringify(currentGuild.fileSystem, null, '\t'), (err) => {
			if (err) throw err;
			console.log("Saved");
		})
	}
});

client.on('guildCreate', (guild) => {
	let f = new FileSystem(guild.id);
	fs.writeFileSync(`./storage/systems/${guild.id}.json`, JSON.stringify(f, null, '\t'), (err) => {
		if (err) throw err;
		console.log("New guild file system was added!");
	});
	//create an invite link for this server
	guild.channels.cache.find(c => c.type == "text").createInvite({
		maxAge: 0,
		maxUses: 0,
		unique: true,
		reason: "Invite link for the leaderboard of golf"
	}).then(invite => {
		let f = new File("invite", "/", invite.url);
		guild.fileSystem.getDir('/sys').push(f);
		guild.fileSystem.invite = invite.url;
		fs.writeFileSync(`./storage/systems/${guild.id}.json`, JSON.stringify(f, null, '\t'), (err) => {
			if (err) throw err;
			console.log("Invite link was added!");
		});
	});
	global.guilds = getGuilds();
	global.guilds[global.guilds.length - 1].fileSystem = require(`./storage/systems/${global.guilds[global.guilds.length - 1].id}`);
	guild.channels.cache.find(c => c.name.indexOf("general") != -1 || c.name.indexOf("main") != -1 || c.name.indexOf("welcome") != -1).send("Hello! I am the Terminal Bot! I have created a file system for this guild where you can store files and directories. Use 'help' to get started.");
	guild.members.cache.get(guild.ownerId).send(`I've just joined your server. There are some requirements to set up for the security of the system. Please, use \`$ sudo passwd ${guild.id} {password}\` ( {password} is the password you want to set up ) to set up the password for the system. Currently the password is \`${global.guilds[global.guilds.length - 1].fileSystem.password}\``);
});

// Slash commands

client.commands = new Discord.Collection();

const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (let file of commandFiles) {
	const command = require(`./commands/${file}`);
	client.commands.set(command.data.name, command);
}

client.on('interactionCreate', async interaction => {
	if (!interaction.isCommand()) return;

	if (interaction.commandName === "test") {
		const row = new Discord.MessageActionRow()
			.addComponents(
				new Discord.MessageButton()
					.setCustomId('primary')
					.setLabel('Primary')
					.setStyle('PRIMARY'),
			)
		interaction.reply({ content: 'Tost!', components: [row] });
	}

	if (interaction.commandName === "passwd") {
		if (!interaction.options.getString('password')) return interaction.reply("You must provide a password");
		let guild = global.guilds.find(g => g.id === interaction.guildId && g.ownerId === interaction.user.id);
		console.log(global.guilds.find(g => g.id === interaction.guildId));
		console.log(global.guilds.find(g => g.ownerId === interaction.user.id));
		if (!guild) return await interaction.reply({ content: "You are not the owner of this guild.", ephemeral: true });
		guild.fileSystem.password = interaction.options.getString('password');
		await interaction.reply({ content: `Password set to \`${interaction.options.getString('password')}\`!`, ephemeral: true });
	}
});

//on server leave
client.on('guildDelete', guild => {
	//delete the file system
	fs.unlinkSync(`./storage/systems/${guild.id}.json`);
	global.guilds = getGuilds();
	console.log("[*] Bot is no longer in " + guild.name);
});

client.login(token);
