const { SlashCommandBuilder } = require('@discordjs/builders');
module.exports = {
    data: new SlashCommandBuilder()
        .setName('passwd')
        .setDescription('Set an SSH password for a guild you own.')
        .addStringOption(o => o.setName('password').setDescription('The password to set.'))
};