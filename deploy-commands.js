const fs = require('fs');
const { REST } = require('@discordjs/rest');
const { Routes } = require('discord-api-types/v9');

require('dotenv').config();
const token = process.env.TOKEN, id = process.env.CLIENT_ID;

// Global commands
const commands = [];
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	commands.push(command.data.toJSON());
}

const rest = new REST({ version: '9' }).setToken(token);

// Global commands
rest.put(Routes.applicationCommands(id), { body: commands })
	.then(() => console.log('Successfully registered global application commands.'))
	.catch(console.error);
